
from trytond.pool import Pool
from . import imaging
from . import configuration
from . import service
from . import evaluation
from .wizard import wizard_imaging


def register():
    Pool.register(
        configuration.HealthConfiguration,
        imaging.TestType,
        imaging.Test,
        imaging.ResultTemplate,
        imaging.TypeResultTemplate,
        imaging.ImagingTestRequest,
        service.ImagingRequestForm,
        service.HealthServiceLine,
        evaluation.PatientEvaluation,
        wizard_imaging.RequestImagingTest,
        wizard_imaging.RequestPatientImagingTestStart,
        module='health_imaging', type_='model')
    Pool.register(
        imaging.ImagingDiagnosisReport,
        module='health_imaging', type_='report')
    Pool.register(
        wizard_imaging.WizardGenerateResult,
        wizard_imaging.RequestPatientImagingTest,
        service.ImagingRequest,
        imaging.EditDiagnosis,
        module='health_imaging', type_='wizard')
